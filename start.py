import os
import glob
import spacy
import graphviz
import matplotlib.pyplot as plt
from xml.etree import cElementTree as ET
from tkinter import Tk, filedialog, Label, Button

__author__ = "6599780, Kai Rene Knöchel"


def get_xml(p):
    """ This function searches all files from the given path and returns a list of all .xml files """
    path1 = p
    path2 = []

    for i in os.walk(path1):
        if glob.glob(os.path.join(i[0], '*.xml')) != []:
            path2 += glob.glob(os.path.join(i[0], '*.xml'))

    return path2


def get_text(xml):
    """ This function expects .xml files and returns the content """
    content = []
    tree = ET.parse(xml)
    root = tree.getroot()
    for child in root:
        if child.tag == "TEXT":
            content.append(child.text)
    return content


def get_token(message):
    """ This function tokenize the text with spacy functions and returns them in a list """

    tokens = []
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(message)
    for token in doc:
        tokens.append([token.text, token.pos_, token.tag_])
    return tokens


def count_pos_tag(token):
    """ This function expects a list and counts how often a PoS-Tag appears"""

    tag_number = [["ADJ", "adjective", 0],
            ["ADP", "adposition", 0],
            ["ADV", "adverb", 0],
            ["AUX", "auxiliary", 0],
            ["verb", "verb", 0],
            ["CONJ", "coordinating conjunction", 0],
            ["DET", "determiner", 0],
            ["INTJ", "interjection", 0],
            ["NOUN", "noun", 0],
            ["NUM", "numeral", 0],
            ["PART", "particle", 0],
            ["PRON", "pronoun", 0],
            ["PROPN", "proper", 0],
            ["noun", "noun", 0],
            ["PUNCT", "punctuation", 0],
            ["SCONJ", "subordinating", 0],
            ["conjunction", "conjunction", 0],
            ["SYM", "symbol", 0],
            ["VERB", "verb", 0],
            ["X", "other", 0]]

    for i in token:
        state = True
        for j in tag_number:
            if i[1] == j[0]:
                j[2] += 1
                break
            else:
                state = False
        if state == False:
            tag_number[len(tag_number)-1][2] += 1

    return tag_number


def get_link_number(xml):
    """ This function expect a .xml file and counts SpatialEntities, Places, Motions, Locations, Signals, QsLinks and OLinks """

    links = [["SPATIAL_ENTITIY", 0], ["PLACE", 0], ["MOTION", 0], ["LOCATION", 0], ["SPATIAL_SIGNAL", 0], ["MOTION_SIGNAL",0], ["QSLINK",0], ["OLINK",0]]
    tree = ET.parse(xml)
    for child in tree.iter():
        for i in links:
            if i[0] == child.tag:
                i[1] += 1
    return links


def get_qs_links(xml):
    """ This function expect a .xml file and returns the type of QsLinks in the file"""

    a = []
    qs_type = []
    tree = ET.parse(xml)
    root = tree.getroot()
    for i in root:
        #  attributes of tags
        for j in i:
            # print(j.tag,j.attrib)
            if j.tag == "QSLINK":
                    a.append(j.attrib["relType"])
    counter = 0
    check = []
    while counter != len(a):
        if a[counter] not in check:
            check.append(a[counter])
            qs_type.append([a[counter], 0])
        counter += 1
    for i in a:
        for j in qs_type:
            if j[0] == i:
                j[1] += 1
    return qs_type


def count_sentence(t):
    """ This function expect a text. It splits the text into sentences and count the length of the sentence"""
    sentences = []
    x = []
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(t)
    for i in doc.sents:
        sentences.append([i, len(i)])

    for i in sentences:
        if i[1] not in x:
            x.append(i[1])
    for i in range(len(x)):
        x[i] = [x[i], 0]

    for i in sentences:
        for j in range(len(x)):
            if i[1] == x[j][0]:
                x[j][1] += 1
    return x


def sentence_graph(k,f):
    """ This function expect a list and visualize a graph"""
    x = []
    y = []
    n = os.path.basename(f).replace('.', '_')  # get file name
    for i in k:
        x.append(i[0])
        y.append(i[1])

    plt.figure(figsize=(len(x), len(y)/2))

    plt.subplot(131)
    plt.bar(x, y)
    plt.xlabel("Satzlänge")
    plt.ylabel("Satzhäufigkeit")

    plt.subplot(133)
    plt.scatter(x,y)

    plt.xlabel("Satzlänge")
    plt.ylabel("Satz Häufigkeit")
    plt.grid(True)
    plt.savefig(n+".png")
    plt.close('all')


def prp_trigger(xml):
    """ This function expect a .xml file and searchs for prepositional triggers (SPATIAL_SIGNAL) of QS- and O-Links"""
    spatial = []
    qs_link = []
    o_link = []

    tree = ET.parse(xml)
    root = tree.getroot()
    for i in root:
        #  attributes of tags
        for j in i:
            # print(j.tag,j.attrib)
            if j.tag == "SPATIAL_SIGNAL":
                spatial.append([j.attrib["text"], j.attrib["id"]])
            if j.tag == "QSLINK":
                qs_link.append([j.attrib["id"], j.attrib["trigger"]])
            if j.tag == "OLINK":
                o_link.append([j.attrib["id"], j.attrib["trigger"]])

    return spatial, qs_link, o_link


def prp_link(spatial,qs_link,o_link):
    """ This function expects Spatial-Signals, QS- and OLinks. It returns how often a Spatial-Signal is triggering a QS- or OLink"""
    count = []
    links = []
    counter = 0
    check = []
    while counter != len(spatial):
        if spatial[counter][0] not in check:
            check.append(spatial[counter][0])
            count.append([spatial[counter][0], 0,[],[]])
        counter += 1
    for i in spatial:
        for j in count:
            if j[0] == i[0]:
                j[1] += 1
                j[2].append(i[1])
                for e in qs_link:
                    if e[1] == i[1]:
                        j[3].append(e[0])
                for k in o_link:
                    if k[1] == i[1]:
                        j[3].append(k[0])

    return count


def count_motion(xml):
    """ This function expect a .xml file and counts the Top 5 MOTIONS"""
    motion = []
    motion2 = []

    tree = ET.parse(xml)
    root = tree.getroot()
    for i in root:
        #  attributes of tags
        for j in i:
            # print(j.tag,j.attrib)
            if j.tag == "MOTION":
                motion.append([j.attrib["text"], j.attrib["id"]])
    counter = 0
    check = []
    while counter != len(motion):
        if motion[counter][0] not in check:
            check.append(motion[counter][0])
            motion2.append([motion[counter][0], 0, []])
        counter += 1
    for i in motion:
        for j in motion2:
            if j[0] == i[0]:
                j[1] += 1
                j[2].append(i[1])

    x = 1
    state = True
    # Bubblesort returns list
    while state == True:  # while list is unsorted
        state = False  # termination condition
        for i in range(len(motion2) - x):
            # if element < neighbor we exchange them
            if motion2[i][1] < motion2[i + 1][1]:
                motion2[i], motion2[i + 1] = motion2[i + 1], motion2[i]
                state = True

    # Top 5 MOTIONS
    y = 0
    motion5 = [[], [], [], [], []]
    for i in motion2:
        if motion5[y] == []:
            motion5[y].append(i)
        elif motion5[y][0][1] == i[1]:
            motion5[y].append(i)
        else:
            y += 1
            motion5[y].append(i)

    return motion5


def write_ans(x):
    """ This function expect a list with our results and creats a .txt file with the results"""
    file = open(os.path.basename(x[3]).replace('.','_') + ".txt", "w")
    #  2.3.1
    file.write("Wie oft kommen welche PoS-Tags vor?\n\n")

    for i in x[0]:
        file.write("PoS-Tag: " + str(i[0]) + "; Name: " + str(i[1]) + "; Anzahl: " + str(i[2]) + "\n")

    file.write("\n")

    #  2.3.2
    file.write("\nWie viele [SpatialEntities, Places, Motions, Locations, Signals, QsLinks, OLinks] gibt es?\n\n")
    for i in x[1]:
        file.write(str(i[0]) + ": " + str(i[1]) + "\n")

    file.write("\n")

    #  2.3.3
    file.write("\nWie oft kommen welche QsLink Typen vor? (DC,EC, ...)?\n\n")
    file.write("QSLinks:\n")
    for i in x[2]:
        file.write(str(i[0]) + ": " + str(i[1]) + "\n")

    file.write("\n")

    #  2.3.4
    file.write("\nVerteilung der Satzlänge graphisch darstellen (x: Satzlänge, y: Wie häufig)?\n\n")
    file.write("look:"+ os.path.basename(x[3]).replace('.','_') +".png" +"\n\n")

    #  2.3.5
    file.write("\nWelche Links (QSLinks, OLinks) werden von welchen Präpositionen (markiert durch\
SPATIAL_SIGNAL) getriggert (z.B. wie oft werden QSLinks durch die Präposition „on“ getriggert)?\n\n")
    for i in x[4]:
        file.write("SPATIAL_SIGNAL" + str(i[0]) + "\n" + "Anzahl: " + str(i[1]) + "\n")
        if i[3] != [] and i[2] != []:
            s = len(i[2])
            t = len(i[3])
            s1 = 0
            t1 =0
            while s != 0 and t != 0:
                if i[3][0][0] == 'q':
                    file.write("QSLink ID: " + str(i[3][t1]) + " wird von SPATIAL_SIGNAL ID: " + str(i[2][s1]) + " getriggert" + "\n")

                if i[3][0][0] == 'o':
                    file.write("OLink ID: " + str(i[3][t1]) + " wird von SPATIAL_SIGNAL ID: " + str(i[2][s1]) + " getriggert" + "\n")
                s -= 1
                t -= 1
            """
            for j in range(len(i[2])):
                if i[3][0][0] == 'q':
                    file.write("QSLink ID: " + str(i[3][j]) + " wird von SPATIAL_SIGNAL ID: " + str(i[2][j]) + " getriggert" + "\n")
                if i[3][0][0] == 'o':
                    file.write("OLink ID: " + str(i[3][j]) + " wird von SPATIAL_SIGNAL ID: " + str(i[2][j]) + " getriggert" + "\n")
            """
            file.write("\n")
    file.write("\n")

    # 2.3.6
    file.write("\nWelches sind die fünf häufigsten „MOTION“ Verben (und wie oft kommen diese vor)?\n\n")
    p = 1
    for i in x[5]:
        file.write("Platz " + str(p) + ":\n")
        p += 1
        for j in i:
            file.write("MOTION: " + str(j[0]) + "; ANZAHL: " + str(j[1]) + "; ID der Motions: "+ str(j[2]) + "\n")
        file.write("\n")
    file.write("\n")


def get_node(xml):
    """ This function expect a .xml file and creats nodes"""

    ent = ["PLACE", "LOCATION", "SPATIAL_ENTITY", "NONMOTION_EVENT", "PATH"]
    nodes = []
    meta_keys = []
    tree = ET.parse(xml)

    # Metalinks toText as keys for dict
    for child in tree.iter():
        if child.tag == "METALINK":
            if child.attrib["toText"] not in meta_keys:
                meta_keys.append(child.attrib["toText"])

    f = []
    for i in range(len(meta_keys)):
        f.append([])
    meta = dict(zip(meta_keys, f))  # meta = {toText:[]}

    # fill the values of our dict meta

    for child in tree.iter():
        if child.tag == "METALINK":
            if child.attrib["toText"] in meta:
                    meta[child.attrib["toText"]].append([child.attrib["toID"], child.attrib["fromID"], child.attrib["fromText"]])

    # meta = {toText:[[toID,fromID,fromText], ...]} um später zu mergen muss man die IDs abspeichern

    for child in tree.iter():
        if child.tag in ent:

            #  find color
            c = 0
            for j in range(len(ent)):
                if child.tag == ent[j]:
                    c = j
                    break

            #  Merge nodes
            x = child
            if x.attrib["text"] in meta:
                for i in meta[x.attrib["text"]]:  # find toText in meta = {toText:[[toID,fromID,fromText], ...]}
                    if i[0] == x.attrib["id"]:  # i[0] = toID in meta = {toText:[[toID,fromID,fromText], ...]}
                        x.attrib["text"] = i[2]  # set text of child = fromText for merge
                        break

            state = True
            for i in nodes:  # if is already in nodes. Find the node and append id
                if i[1] == x.attrib["text"]:
                    i[2].append(x.attrib["id"])
                    state = False

            if state == True:  # else we create a new node
                nodes.append([x.tag, x.attrib["text"], [x.attrib["id"]], c])
    # nodes = [[Name,Text, [ID, ...], color], ...]
    return nodes


def get_edges(nodes, xml):
    """ This function expect a .xml file and nodes. It returns the edges between nodes in a list"""
    edges = []
    link = []
    tree = ET.parse(xml)
    for child in tree.iter():
        if child.tag == "QSLINK":
            link.append([child.tag, child.attrib, "yellow"])
        if child.tag == "OLINK":
            link.append([child.tag, child.attrib, "red"])

    for i in link:
        start = ""
        end = ""
        for j in nodes:
            state = False
            for e in j[2]:
                if i[1]["fromID"] == e:
                    start = j[1]
                    state = True
                    break
            if state:
                break

        for j in nodes:
            state = False
            for e in j[2]:
                if i[1]["toID"] == e:
                    end = j[1]
                    state = True
                    break
            if state:
                break
        edges.append([i[1]["relType"],start,end,i[2]])
    return edges


def visual_graph(nodes,edges,n):
    # ["PLACE", "LOCATION", "SPATIAL_ENTITY", "NONMOTION_EVENT", "PATH"] -> [red, yellow, chartreuse, aqua, violet]
    col1 = ["red", "yellow", "chartreuse", "lightblue1", "magenta"]
    ent = ["PLACE", "LOCATION", "SPATIAL_ENTITY", "NONMOTION_EVENT", "PATH"]
    col = []
    # nodes = [[Name, text, id, color_index], ...]
    for i in nodes:
        for j in range(len(ent)):
            if ent[j] == i[0]:
                col.append(col1[j])
                break

    # crate graph nodes
    dot = graphviz.Digraph(comment="name")

    for i in range(len(nodes)):
        dot.attr('node',style='filled', color= col[i])
        dot.node(nodes[i][1], nodes[i][1])

    for j in edges:
        dot.attr('edge', color= j[3])
        dot.edge(j[1], j[2], label= j[0])

    dot.render(filename="Aufgabe2_4/" + os.path.basename(n).replace('.','_') + ".gv", view=True)


def aufgabe23(state):
    root = Tk()
    root.withdraw()  # hide tkinter window
    if state:

        folder = filedialog.askdirectory().replace(os.sep, "/")  # select folder with trainingdata
        x1 = get_xml(folder)  # get xml
    else:
        f = filedialog.askopenfilename().replace(os.sep, "/")
        x1 = [f]

    for i in range(len(x1)):
        x2 = get_text(x1[i])  # get text for token
        x3 = get_token(x2[0])  # Token [Text, PoS, tag]
        x4 = count_pos_tag(x3)  # Aufgabe 2.3.1.: PoS-Tags number [PoS,Name,number].
        x5 = get_link_number(
            x1[i])  # Aufgabe 2.3.2.: number of SpatialEntities, Places, Motions, Locations, Signals, QsLinks, OLinks.
        x6 = get_qs_links(x1[i])  # Aufgabe 2.3.3.: get QSLink number and types.
        x7 = count_sentence(x2[0])  # Aufgabe 2.3.4.: split text in sentences, count sentence length.
        x8 = sentence_graph(x7, x1[i])
        x9 = prp_trigger(x1[i])
        x10 = prp_link(x9[0], x9[1], x9[2])  # Aufgabe 2.3.5.: [word,number,id,link].
        x11 = count_motion(x1[i])  # Aufgabe 2.3.6.: Top 5 Motions
        x = [x4, x5, x6, x1[i], x10, x11]
        write_ans(x)


def aufgabe24():
    root = Tk()
    root.withdraw()  # hide tkinter window
    f = filedialog.askopenfilename().replace(os.sep, "/")
    x12 = get_node(f)
    x13 = get_edges(x12, f)
    visual_graph(x12, x13, f)


def main():

    root = Tk()
    root.geometry("480x280")

    # Labels in window
    lab = Label(root, text="Aufgabe 2.3" \
                   , anchor="nw", font=('Century Gothic', 10))
    lab.place(x=20, y=30, width=500, height=25)

    lab2 = Label(root, text="Was möchten sie einlesen?" \
                , anchor="nw", font=('Century Gothic', 10))
    lab2.place(x=20, y=60, width=500, height=25)

    lab3 = Label(root, text="Aufgabe 2.4" \
                 , anchor="nw", font=('Century Gothic', 10))
    lab3.place(x=20, y=150, width=500, height=25)

    lab4 = Label(root, text="Wählen Sie die zu bearbeitende XML-Datei aus" \
                 , anchor="nw", font=('Century Gothic', 10))
    lab4.place(x=20, y=180, width=500, height=25)

    # Buttons in window
    p1 = Button(root, text="Datei", command=lambda: aufgabe23(False), \
                   height=3, width=20, bg="#6E6E6E", font=('Arial Black', 7))
    p1.place(x=40, y=90, width=120, height=25)

    p2 = Button(root, text="Ordner", command=lambda: aufgabe23(True), \
                   height=3, width=20, bg="#6E6E6E", font=('Arial Black', 7))
    p2.place(x=190, y=90, width=120, height=25)

    p3 = Button(root, text="Datei", command=lambda: aufgabe24(), \
                height=3, width=20, bg="#6E6E6E", font=('Arial Black', 7))
    p3.place(x=40, y=210, width=120, height=25)

    root.mainloop()


if __name__ == "__main__":
    main()