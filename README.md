# Fingerübung Kai rene Knöchel 6599780

<p>Um das Programm start.py auszuführen muss ein Python-Interpreter 3.9 oder neuer mit den Bibliotheken und Support Programmen der  <br> 
<p>Standard-Installation von www.python.org. installiert sein. <br> 
<p>Für das Ausführen des Programms müssen weiter Module wie Spacy, Mathplotlib und Graphviz installiert werden. <br>
<p>Dies kann man ausführen mit den Befehlen im Windows Terminal: <br>
<p><br>
<p>python -m pip install -U pip <br>
<p>python -m pip install -U spacy <br>
<p>python -m pip install -U matplotlib <br>
<p>python -m pip install -U graphviz <br>
<p><br>
<p>Wenn das Modul graphviz nicht vollständig funktioniert, sollte man es am besten mit conda nochmal installieren. <br>
<p>Falls dies auch nicht funktioniert, finden Sie hier eine kurze Anleitung damit das System das Modul findet: <br>
<p>https://www.youtube.com/watch?v=q7PzqbKUm_4 <br>
<p>Das Programm wurde bis jetzt nur für Windowssysteme ausgelegt und getestet. <br>
